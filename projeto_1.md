---
layout: page
image: /arduinoweb/img/Cool Waasa-Bruticus.png
title: Piscando LEDs em tempos variados
toc: true
hide_hero: false
toc_title: Piscando LEDs em tempos variados
show_sidebar: false
---
## Objetivo
  Piscar cada LED com a diferença de 1 segundo ao anterior. 

## Materiais utilizados

 - 1 LED vermelho
 - 1 LED amarelo
 - 1 LED verde
 - 1 LED azul
 - 4 Resistores 150 Ω
 - 6 Jumpers
 - 1 Arduino uno R3

 

## Esquema Arduino
![esquema arduino](/arduinoweb/img/Cool Waasa-Bruticus.png)
  
  
## Código 
```
void setup()
{
  pinMode(7, OUTPUT);//define o pino 7 como saída
  pinMode(6, OUTPUT);//define o pino 6 como saída
  pinMode(5, OUTPUT);//define o pino 5 como saída
  pinMode(4, OUTPUT);//define o pino 4 como saída
}

void loop()
{
  digitalWrite(7, HIGH);//acende o led que esta concectado ao pino 7
  delay(1000); // Espera por 1000 milisegundos(1 segundo)
  digitalWrite(7, LOW); //apaga o led que esta concectado ao pino 7
  digitalWrite(6, HIGH);//acende o led que esta concectado ao pino 6
  delay(1000); 
  digitalWrite(6, LOW);
  digitalWrite(5, HIGH);
  delay(1000); 
  digitalWrite(5, LOW);
  digitalWrite(4, HIGH);
  delay(1000); 
  digitalWrite(4, LOW);
  delay(1000);
  
}
```
## Tutorial
<!-- blank line -->
<figure class="image is-16by9">
  <iframe class="has-ratio" src=" https://www.youtube.com/embed/BK9NJYCZcaE"  frameborder="0" allowfullscreen=""> </iframe>
</figure>
<!-- blank line -->


## Simulador
<figure class="image is-16by9">
  <iframe class="has-ratio" src="https://www.tinkercad.com/embed/lblXDZnCRCy?editbtn=1" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
  </figure>