import os
from PIL import Image

cursos = os.listdir('src/assets/img/cursos')


img = [''] * len(cursos)

for i, ano in enumerate(cursos):
    img[i] = os.listdir('src/assets/img/cursos/'+ano)

for ano in img:
    for fichier in ano[:]:
        if not(fichier.endswith((".jpg", ".png"))):
            ano.remove(fichier)

ano_i = 3

MAX_SIZE = (360, 360)

for ano_i, ano in enumerate(cursos):
    print('const photos' + str(ano_i) + ' = [')
    for i, ref in enumerate(img[ano_i]):
        src = 'C:/GitHub/arduinoweb/arduido-web/src/assets/img/cursos/' + \
            cursos[ano_i] + '/' + ref
        image = Image.open(src)
        image.thumbnail(MAX_SIZE)
        image.save(
            'C:/GitHub/arduinoweb/arduido-web/src/assets/img/thumbs/' + cursos[ano_i] + '/thumb_'+ref)
        print('{\n\tsrc: require("assets/img/thumbs/' + cursos[ano_i] + '/thumb_' + ref + '"), \n\tfullscreen: require("assets/img/cursos/' +
              cursos[ano_i] + '/' + ref + '"), \n\twidth: 4, \n\theight: 3\n}' + (',' if i+1 != len(img[ano_i]) else '];\n'))
