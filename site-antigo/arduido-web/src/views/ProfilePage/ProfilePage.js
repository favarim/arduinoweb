import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";

import styles from "assets/jss/material-kit-react/views/profilePage.js";

import mateus from "assets/img/faces/mat2.jpg";
import favarim from "assets/img/faces/fav1.jpg";

const useStyles = makeStyles(styles);

export default function ProfilePage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);
  return (
    <div>
      <Header
        color="transparent"
        brand="Contato"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 200,
          color: "white",
        }}
        {...rest}
      />
      <Parallax small filter image={require("assets/img/profile-bg.jpg")} />
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div>
          <div className={classes.container}>
            <GridContainer justify="center">
              <GridItem xs={6} sm={6} md={6}>
                <div className={classes.profile}>
                  <div>
                    <img src={mateus} alt="..." className={imageClasses} />
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>Mateus Marochi Olenik</h3>
                    <h6>BOLSISTA</h6>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href="https://twitter.com/amarokui"
                    >
                      <i className={classes.socials + " fab fa-twitter"} />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href="https://www.instagram.com/mateusmarochi/"
                    >
                      <i className={classes.socials + " fab fa-instagram"} />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href="https://www.facebook.com/mateus.marochiolenik"
                    >
                      <i className={classes.socials + " fab fa-facebook"} />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href="https://www.linkedin.com/in/mateus-marochi-olenik"
                    >
                      <i className={classes.socials + " fab fa-linkedin"} />
                    </Button>
                  </div>
                </div>
              </GridItem>
              <GridItem xs={6} sm={6} md={6}>
                <div className={classes.profile}>
                  <div>
                    <img src={favarim} alt="..." className={imageClasses} />
                  </div>
                  <div className={classes.name}>
                    <h3 className={classes.title}>Fábio Favarim</h3>
                    <h6>ORIENTADOR</h6>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href="https://www.linkedin.com/in/fabio-favarim-9a3422/"
                    >
                      <i className={classes.socials + " fab fa-linkedin"} />
                    </Button>
                    <Button
                      justIcon
                      color="transparent"
                      className={classes.margin5}
                      href="https://www.facebook.com/fabio.favarim"
                    >
                      <i className={classes.socials + " fab fa-facebook"} />
                    </Button>
                  </div>
                </div>
              </GridItem>
            </GridContainer>
            <div className={classes.description}>
              <p>
                Imagine aqui um texto bem simples incentivando o usuário a
                mandar um e-mail para equipe.{" "}
              </p>
              <br />
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}
