import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";

import styles from "assets/jss/material-kit-react/views/landingPageSections/teamStyle.js";

import mateus from "assets/img/faces/mat2.jpg";
import favarim from "assets/img/faces/fav1.jpg";
import allan from "assets/img/faces/allan.png";
import felipe from "assets/img/faces/felipe.jpg";

const useStyles = makeStyles(styles);

export default function TeamSection() {
  const classes = useStyles();
  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  return (
    <div className={classes.section}>
      <h2 className={classes.title}>Esse é o nosso time</h2>
      <div>
        <GridContainer>
          <GridItem xs={12} sm={6} md={3}>
            <Card plain>
              <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                <img src={mateus} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Mateus Marochi Olenik
                <br />
                <small className={classes.smallTitle}>Bolsista</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Imagine aqui um teto aleatório que você possívelmente ignorou.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://twitter.com/amarokui"
                >
                  <i className={classes.socials + " fab fa-twitter"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.instagram.com/mateusmarochi/"
                >
                  <i className={classes.socials + " fab fa-instagram"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.facebook.com/mateus.marochiolenik"
                >
                  <i className={classes.socials + " fab fa-facebook"} />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card plain>
              <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                <img src={favarim} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Fábio Favarim
                <br />
                <small className={classes.smallTitle}>Orientador</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Imagine aqui outro texto bem elaborado sobre a pessoa acima.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.facebook.com/fabio.favarim"
                >
                  <i className={classes.socials + " fab fa-facebook"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card plain>
              <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                <img src={allan} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Allan Ribeiro
                <br />
                <small className={classes.smallTitle}>Voluntário</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  "Queria ser um líquido" <br /> Ribeiro, 2020
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.youtube.com/watch?v=qTsaS1Tm-Ic"
                >
                  <i className={classes.socials + " fab fa-youtube"} />
                </Button>
                <Button
                  href="https://gitlab.com/kresserbash"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-github"} />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card plain>
              <GridItem xs={12} sm={12} md={12} className={classes.itemGrid}>
                <img src={felipe} alt="..." className={imageClasses} />
              </GridItem>
              <h4 className={classes.cardTitle}>
                Felipe Coltri
                <br />
                <small className={classes.smallTitle}>Voluntário</small>
              </h4>
              <CardBody>
                <p className={classes.description}>
                  Imagine aqui outro texto bem elaborado, pensnado bem, não tão
                  elaborado assim.
                </p>
              </CardBody>
              <CardFooter className={classes.justifyCenter}>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.youtube.com/watch?v=qTsaS1Tm-Ic"
                >
                  <i className={classes.socials + " fab fa-youtube"} />
                </Button>
                <Button
                  href="https://www.linkedin.com/in/felipecoltri/"
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    </div>
  );
}
