import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import InfoArea from "components/InfoArea/InfoArea.js";

import styles from "assets/jss/material-kit-react/views/landingPageSections/productStyle.js";

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <div className={classes.section}>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <h2 className={classes.title}>Informações</h2>
          <h4 className={classes.description}>
            <b>Projeto de Extensão</b>:Uso De Arduino Com Alunos Do Ensino Médio: Uma Perspectiva Na Motivação Para Ingresso Em Cursos De Engenharia Da UTFPR.
            <br /><b>Orientador</b>: Prof. Dr. Fábio Favarim
            <br /><b>Bolsista</b>: Mateus Marochi Olenik
          </h4>
          <h2 className={classes.title}>Caracterização do Problema</h2>
          <h4 className={classes.description}>
            A área das engenharias é uma das que mais tem sofrido com a escassez de profissionais qualificados, nas suas diversas especialidades, o que inclui o Engenheiro de Computação. Pesquisas realizadas nos últimos anos têm demonstrado que esse déficit tem aumentando.
            <br /><br />Entre as áreas de engenharia que mais tem crescido e tido maior demanda é a área de eletrônica. Isso é devido ao importante papel dos microcontroladores na vida das pessoas, visto que estão integrados em diferentes tipos de equipamentos eletrônicos, eletrodomésticos, brinquedos, equipamentos médicos, entre outros. Grande parte desse crescimento se deve a aplicação dos microcontroladores em sistemas embarcados.
            <br /><br />Verificou-se a oportunidade de utilizar o Kit de Desenvolvimento Arduino para a introdução aos sistemas microcontrolados de maneira descomplicada a alunos de escolas de ensino médio, futuros universitários, através do uso do kit de desenvolvimento Arduino. Com o desenvolvimento de pequenos projetos pretende-se motivar os alunos ao ingresso em cursos de engenharia, principalmente de Computação, despertando ou ainda reforçando o interesse pela área de computação. Além disso, pretende-se mostrar aos participantes a importância de disciplinas da área de exatas, pois o desenvolvimento de tais projetos depende desses conhecimentos.</h4>
          <h2 className={classes.title}>Objetivos</h2>
          <h4 className={classes.description}>
            O objetivo geral deste projeto é a motivação de alunos do ensino médio ao ingresso no ensino superior, principalmente, nas engenharias da área de computação, através do desenvolvimento de pequenos projetos de eletrônica com o kit de desenvolvimento Arduino.
            <br /><br />Dentre os objetivos específicos pretendidos com esse projeto destacam-se:
            <br /><br /><b>a)</b> Utilizar o Kit de desenvolvimento Arduino para incentivar e motivar alunos de escolas públicas ao estudo e desenvolvimento de tecnologias.
            <br /><b>b)</b> Desmitificar o desenvolvimento de dispositivos microcontrolados;
            <br /><b>c)</b> Informar e conscientizar alunos do Ensino Médio do conteúdo dos cursos de Engenharia, que enfatizam ciências exatas e exigem dedicação, mas são muitos promissores em termos de oportunidades de mercado;
            <br /><b>d)</b> Despertar nos alunos bolsistas deste projeto a sua corresponsabilidade social;
            <br /><b>e)</b> Contribuir para a curricularização da extensão na graduação;
          </h4>
          <h2 className={classes.title}>Justificativa</h2>
          <h4 className={classes.description}>Tendo em vista a necessidade de profissionais - na região, no país e no mundo – na área de engenharia, principalmente na área de computação, e a baixa procura dos jovens da região por esse tipo de curso, foi que cursos que possam mostrar a atuação do profissional, assim como as oportunidade de trabalho, é proposto esse curso.</h4>
          <h2 className={classes.title}>Métodos e Procedimentos</h2>
          <h4 className={classes.description}>
            A metodologia de realização do curso é baseada em atividades presenciais, com ênfase em prática, na qual sempre será feito através do desenvolvimento de pequenos projetos com a sua dificuldade sendo incrementada. O curso será ministrado por um aluno e pelo coordenador da ação. A professora vice coordenadora da ação auxiliará nos aspectos pedagógicos e operacionais do curso.
            <br /><br />As aulas possuem controle de presença e de acompanhamento do aluno nas atividades.
            <br /><br />Antes de ocorrer o curso, o bolsista que também irá ministrar o curso em conjunto com o coordenador, será preparados para atuar na prática pedagógica e na confecção do material didático das atividades realizadas.
          </h4>
        </GridItem>
      </GridContainer>
    </div>
  );
}
