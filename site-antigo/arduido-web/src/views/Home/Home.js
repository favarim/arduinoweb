import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// @material-ui/icons

// core components
import Header from "components/Header/Header.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Button from "components/CustomButtons/Button.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Parallax from "components/Parallax/Parallax.js";

import { Link } from "react-router-dom";

import styles from "assets/jss/material-kit-react/views/landingPage.js";

// Sections for this page

import TeamSection from "./Sections/TeamSection.js";
import Apresentacao from "./Sections/Apresentacao.js";

const dashboardRoutes = [];

const useStyles = makeStyles(styles);

export default function LandingPage(props) {
  const classes = useStyles();
  const { ...rest } = props;
  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Página Inicial"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <Parallax filter image={require("assets/img/landing-bg-2.jpg")}>
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>Arduino Web</h1>
              <h4>
                Esse material tem sido desenvolvido por acadêmicos bolsistas do
                projeto de extensão intitulado: "Uso De Arduino Com Alunos Do
                Ensino Médio: Uma Perspectiva Na Motivação Para Ingresso Em
                Cursos De Engenharia Da UTFPR".
              </h4>
              <br />
              <Button
                color="danger"
                size="lg"
                href="https://www.youtube.com/watch?v=6GCMwjRcGgY"
                target="_blank"
                rel="noopener noreferrer"
              >
                <i className="fas fa-play" />
                Assista ao Vídeo
              </Button>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <Apresentacao />
          <TeamSection />
        </div>
      </div>
      <Footer />
    </div>
  );
}
