import React from "react";

import classNames from "classnames";
import { makeStyles } from "@material-ui/core/styles";

import Header from "components/Header/Header.js";
import HeaderLinks from "components/Header/HeaderLinks.js";
import Footer from "components/Footer/Footer.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";
import Parallax from "components/Parallax/Parallax.js";
import Button from "components/CustomButtons/Button.js";
import DiscreteSlider from "components/DiscreteSlider/DiscreteSlider.js";

import PhotoGallery from "views/CursosAnteriores/Sections/Gallery.js";

import styles from "assets/jss/material-kit-react/views/cursosAnteriores.js";

const useStyles = makeStyles(styles);
const dashboardRoutes = [];

export default function CursosAnteriores(props) {
  const classes = useStyles();
  const { ...rest } = props;
  var value;

  const changeGallery = () => {
    value = 1;
  };

  return (
    <div>
      <Header
        color="transparent"
        routes={dashboardRoutes}
        brand="Cursos Anteriores"
        rightLinks={<HeaderLinks />}
        fixed
        changeColorOnScroll={{
          height: 400,
          color: "white",
        }}
        {...rest}
      />
      <Parallax
        className={classes.parallax}
        filter
        image={require("assets/img/arduino-classroom.jpeg")}
      >
        <div className={classes.container}>
          <GridContainer>
            <GridItem xs={12} sm={12} md={6}>
              <h1 className={classes.title}>Cursos Anteriores</h1>
              <h4>
                Esse material tem sido desenvolvido por acadêmicos bolsistas do
                projeto de extensão intitulado: "Uso De Arduino Com Alunos Do
                Ensino Médio: Uma Perspectiva Na Motivação Para Ingresso Em
                Cursos De Engenharia Da UTFPR".
              </h4>
            </GridItem>
          </GridContainer>
        </div>
      </Parallax>
      <div className={classNames(classes.main, classes.mainRaised)}>
        <div className={classes.container}>
          <DiscreteSlider />
        </div>
      </div>
      <Footer />
    </div>
  );
}
