import React from "react";
// material-ui components
import { makeStyles } from "@material-ui/core/styles";
// core components
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";

import styles from "assets/jss/material-kit-react/views/components.js";

const useStyles = makeStyles(styles);

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var cardsImage = ["", "", "", "", "", "", "", "", ""];
{
  cardsImage = cardsImage.map(function() {
    return (
      "https://picsum.photos/id/" +
      Math.round(Math.random() * 1024) +
      "/320/180"
    );
  });
}
export default function PhotoGalery() {
  const classes = useStyles();
  return (
    <Container className={classes.cardGrid} maxWidth="md">
      {/* End hero unit */}
      <Grid container spacing={4}>
        {console.log(cards.map)}
        {cards.map(card => (
          <Grid item key={card} xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardMedia
                className={classes.cardMedia}
                image={cardsImage[card]}
                title={card}
              />
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
