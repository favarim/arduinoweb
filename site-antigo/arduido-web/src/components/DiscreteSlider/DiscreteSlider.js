import React from "react";
import { withStyles, makeStyles, useTheme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import PhotoGallery from "views/CursosAnteriores/Sections/Gallery.js";
import SwipeableViews from "react-swipeable-views";

import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import classNames from "classnames";

import mateus from "assets/img/faces/mat2.jpg";

import imagesStyle from "assets/jss/material-kit-react/imagesStyles.js";

const photos0 = [
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20171125_114219.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20171125_114219.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20171209_102733.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20171209_102733.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20171216_101913.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20171216_101913.jpg"),
    width: 3,
    height: 4,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20180131_150010.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20180131_150010.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20180505_163934.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20180505_163934.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20180811_124251.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20180811_124251.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20180818_111755.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20180818_111755.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20180818_111808.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20180818_111808.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20180818_111831.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20180818_111831.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-1/thumb_IMG_20180818_111841.jpg"),
    fullscreen: require("assets/img/cursos/2018-1/IMG_20180818_111841.jpg"),
    width: 4,
    height: 3,
  },
];

const photos1 = [
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181027_101559027.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181027_101559027.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181027_101615613.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181027_101615613.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181027_101708296.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181027_101708296.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181027_105704123.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181027_105704123.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110137307_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110137307_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110143179_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110143179_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110226132_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110226132_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110233378.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110233378.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110245559_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110245559_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110248129_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110248129_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110347731.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110347731.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2018-2/thumb_IMG_20181124_110355758.jpg"),
    fullscreen: require("assets/img/cursos/2018-2/IMG_20181124_110355758.jpg"),
    width: 4,
    height: 3,
  },
];

const photos2 = [
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190521_113409236.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190521_113409236.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190521_114234739_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190521_114234739_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190521_114237964.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190521_114237964.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190521_114243141_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190521_114243141_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190523_120204608.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190523_120204608.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190606_105726443.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190606_105726443.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190606_105733676.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190606_105733676.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190606_105745941.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190606_105745941.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-1/thumb_IMG_20190606_105753705.jpg"),
    fullscreen: require("assets/img/cursos/2019-1/IMG_20190606_105753705.jpg"),
    width: 4,
    height: 3,
  },
];

const photos3 = [
  {
    src: require("assets/img/thumbs/2019-2/thumb_IMG_20190914_161939099.jpg"),
    fullscreen: require("assets/img/cursos/2019-2/IMG_20190914_161939099.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-2/thumb_IMG_20190914_161946708.jpg"),
    fullscreen: require("assets/img/cursos/2019-2/IMG_20190914_161946708.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-2/thumb_IMG_20190920_183627094.jpg"),
    fullscreen: require("assets/img/cursos/2019-2/IMG_20190920_183627094.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-2/thumb_IMG_20190921_161056760.jpg"),
    fullscreen: require("assets/img/cursos/2019-2/IMG_20190921_161056760.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-2/thumb_IMG_20190921_161806495.jpg"),
    fullscreen: require("assets/img/cursos/2019-2/IMG_20190921_161806495.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-2/thumb_IMG_20190921_162854599.jpg"),
    fullscreen: require("assets/img/cursos/2019-2/IMG_20190921_162854599.jpg"),
    width: 4,
    height: 3,
  },
  {
    src: require("assets/img/thumbs/2019-2/thumb_IMG_20191108_170146630_BURST000_COVER_TOP.jpg"),
    fullscreen: require("assets/img/cursos/2019-2/IMG_20191108_170146630_BURST000_COVER_TOP.jpg"),
    width: 4,
    height: 3,
  },
];

const useStyles = makeStyles({
  root: {
    padding: "14px 0 18px 0",
    textAlign: "center",
    width: "wd",
  },
  title: {
    color: "#3C4858",
  },
  ...imagesStyle,
});

const YearsSlider = withStyles({
  root: {
    color: "#0097A7",
    height: 8,
    width: "85%",
    align: "center",
    padding: "0 0 30px 0",
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: "#fff",
    border: "2px solid currentColor",
    marginTop: -8,
    marginLeft: -12,
    "&:focus, &:hover, &$active": {
      boxShadow: "inherit",
    },
  },
  active: {},
  valueLabel: {
    left: "calc(-50% + 4px)",
  },
  track: {
    height: 8,
    borderRadius: 4,
  },
  rail: {
    height: 8,
    borderRadius: 4,
  },
})(Slider);

function valuetext(value) {
  return `${value}°C`;
}

function CreateGallery(props) {
  const { value, index, photos, ...other } = props;
  return (
    <div hidden={value !== index}>
      <PhotoGallery photos={photos} />
    </div>
  );
}

export default function DiscreteSlider(props) {
  const classes = useStyles();
  const theme = useTheme();

  const imageClasses = classNames(
    classes.imgRaised,
    classes.imgRoundedCircle,
    classes.imgFluid
  );
  const navImageClasses = classNames(classes.imgRounded, classes.imgGallery);

  const { ...rest } = props;
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const marks = [
    {
      value: 0,
      label: "2018.1",
    },
    {
      value: 1,
      label: "2018.2",
    },
    {
      value: 2,
      label: "2019.1",
    },
    {
      value: 3,
      label: "2019.2",
    },
  ];

  return (
    <div className={classes.root} value={value}>
      <Typography id="discrete-slider" gutterBottom>
        <h2 className={classes.title}>Cursos Anteriores</h2>
      </Typography>
      <YearsSlider
        defaultValue={marks.length}
        getAriaValueText={valuetext}
        aria-labelledby="discrete-slider"
        valueLabelDisplay="off"
        step={null}
        marks={marks}
        min={0}
        max={marks.length - 1}
        value={value}
        onChange={handleChange}
        {...rest}
      />
      <SwipeableViews
        axis={theme.direction === "rtl" ? "x-reverse" : "x"}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <CreateGallery
          value={value}
          index={0}
          dir={theme.direction}
          photos={photos0}
        />
        <CreateGallery
          value={value}
          index={1}
          dir={theme.direction}
          photos={photos1}
        />
        <CreateGallery
          value={value}
          index={2}
          dir={theme.direction}
          photos={photos2}
        />
        <CreateGallery
          value={value}
          index={3}
          dir={theme.direction}
          photos={photos3}
        />
        <GridContainer value={value} index={0} justify="center">
          <GridItem xs={6} sm={6} md={6}>
            <div className={classes.profile}>
              <div>
                <img src={mateus} alt="..." className={imageClasses} />
              </div>
              <div className={classes.name}>
                <h3 className={classes.title}>Mateus Marochi Olenik</h3>
                <h6>BOLSISTA</h6>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://twitter.com/amarokui"
                >
                  <i className={classes.socials + " fab fa-twitter"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.instagram.com/mateusmarochi/"
                >
                  <i className={classes.socials + " fab fa-instagram"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.facebook.com/mateus.marochiolenik"
                >
                  <i className={classes.socials + " fab fa-facebook"} />
                </Button>
                <Button
                  justIcon
                  color="transparent"
                  className={classes.margin5}
                  href="https://www.linkedin.com/in/mateus-marochi-olenik"
                >
                  <i className={classes.socials + " fab fa-linkedin"} />
                </Button>
              </div>
            </div>
          </GridItem>
        </GridContainer>
      </SwipeableViews>
    </div>
  );
}
