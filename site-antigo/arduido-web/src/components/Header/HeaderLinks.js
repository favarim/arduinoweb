/*eslint-disable*/
import React from "react";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from "@material-ui/core/IconButton";
// react components for routing our app without refresh
import { Link } from "react-router-dom";

// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Tooltip from "@material-ui/core/Tooltip";

// @material-ui/icons
import HomeIcon from "@material-ui/icons/Home";
import SchoolIcon from "@material-ui/icons/School";
import BookIcon from "@material-ui/icons/Book";
import MailIcon from "@material-ui/icons/Mail";

// core components
import CustomDropdown from "components/CustomDropdown/CustomDropdown.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-kit-react/components/headerLinksStyle.js";

const useStyles = makeStyles(styles);

export default function HeaderLinks(props) {
  const classes = useStyles();
  return (
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Link to="/" className={classes.menuLink}>
          <Button
            href="/"
            color="transparent"
            target="_blank"
            className={classes.navLink}
          >
            <HomeIcon className={classes.icons} />
            Página Inicial
          </Button>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <CustomDropdown
          noLiPadding
          buttonText="Cursos de Arduino"
          buttonProps={{
            className: classes.navLink,
            color: "transparent",
          }}
          buttonIcon={SchoolIcon}
          dropdownList={[
            <Link to="/" className={classes.dropdownLink}>
              Módulo Básico
            </Link>,
            <Link to="/" className={classes.dropdownLink}>
              Módulo Intermediário
            </Link>,
          ]}
        />
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link to="/cursos-anteriores" className={classes.menuLink}>
          <Button
            to="/cursos-anteriores"
            color="transparent"
            target="_blank"
            className={classes.navLink}
          >
            <BookIcon className={classes.icons} /> Cursos Anteriores
          </Button>
        </Link>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Link to="/contato" className={classes.menuLink}>
          <Button
            color="transparent"
            target="_blank"
            className={classes.navLink}
          >
            <MailIcon className={classes.icons} />
            Contato
          </Button>
        </Link>
      </ListItem>
    </List>
  );
}
