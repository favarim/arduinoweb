---

layout: post
title: Exemplo
summary: |-
    esse é um post de exemplo.

published: true
---

Este é o texto da postagem. Ele não será usado como um trecho, pois, em vez disso, usará o resumo personalizado definido na postagem.

Se o resumo não for definido, ele usará o trecho padrão gerado pelo Jekyll.

Para usar um resumo personalizado, defina o `resumo` na capa do post. 

```yaml
Este é o texto da postagem. Ele não será usado como um trecho, pois, em vez disso, usará o resumo personalizado definido na postagem.

Se o resumo não for definido, ele usará o trecho padrão gerado pelo Jekyll.

Para usar um resumo personalizado, defina o `resumo` na capa do post.
```