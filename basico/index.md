---
layout: page
title: Módulo básico
subtitle: Apostila módulo básico
toc: true
hide_hero: true
toc_title: Módulo básico
show_sidebar: false
---
## Conteúdos abordados

Alguns dos tópicos abordados na apostila.

* Introdução a plataforma Arduino
* Eletrônica básica
* Saída digital
* Entrada digital
* Entrada Analógica
* Sinal PWM
* Comunicação Serial
* Diversos sensores e componentes eletrônicos
* Muitos Projetos    

## Video-Aulas 
    Video aulas gravadas pelos intrutores explicando os principais conceitos e montando os projetos 
<figure class="image is-16by9">
  <iframe class="has-ratio" src=" https://www.youtube.com/embed/fI83-EThwjU"
  frameborder="0" allowfullscreen=""> </iframe>
</figure>

Para acessar a Playlist Completa [CLIQUE AQUI](https://www.youtube.com/watch?v=54EQQNQIeBM&list=PLQHbCTTyB-vyuUfO51NA5xuMFk-IvMlK6)
<!-- blank line -->

## Apostila
<!-- blank line -->
Para baixar a apostila [CLIQUE AQUI](../apostila.pdf)